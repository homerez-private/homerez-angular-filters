'use strict';
describe('orderByTranslated', function() {
  var translations = {
      'AMENITY_EQUIPMENT_AIR_CONDITIONING'  : 'Aire Acondicionado',
      'AMENITY_EQUIPMENT_WASHING_MACHINE'   : 'Lavadora',
      'AMENITY_EQUIPMENT_DRYER'             : 'Secadora',
      'AMENITY_EQUIPMENT_DRESSING_ROOM'     : 'Vestidor',
      'AMENITY_EQUIPMENT_FAN'               : 'Ventilador',
      'AMENITY_EQUIPMENT_INDOOR_FIREPLACE'  : 'HOGAR',
      'AMENITY_EQUIPMENT_CENTRAL_HEATING'   : 'Calefacción Central',
      'AMENITY_EQUIPMENT_IRON_IRONING_TABLE': 'Plancha / Tabla de Planchar',
      'AMENITY_EQUIPMENT_HOT_TUB'           : 'Hidromasaje',
      'AMENITY_EQUIPMENT_MOSQUITO_NET'      : 'Mosquitera',
      'AMENITY_EQUIPMENT_SAFE'              : 'Caja fuerte',
      'AMENITY_EQUIPMENT_SOFA'              : 'Sofá',
      'AMENITY_EQUIPMENT_DESK'              : 'Escritorio',
      'AMENITY_EQUIPMENT_CLEANING_PRODUCTS' : 'Productos de limpieza',
      'AMENITY_EQUIPMENT_VACUUM_CLEANER'    : 'Aspiradora'
  };

  var translationKeys = Object.keys(translations);

  beforeEach(module('homerez.filters'));

  describe('orderByTranslated in English', function() {
    beforeEach(inject(function(_$translate_) {
      sinon.stub(_$translate_, 'instant', function(key) {return translations[key]});
    }));

    it('should return as many results as the original list does',
      inject(function(orderByTranslatedFilter) {
        var result = orderByTranslatedFilter(translationKeys);
        expect(result).to.have.length(translationKeys.length);
    }));

    it('should show "Aspiradora" before "Ventilador"',
      inject(function(orderByTranslatedFilter) {
        var result = orderByTranslatedFilter(translationKeys);
        expect(result.indexOf('AMENITY_EQUIPMENT_VACUUM_CLEANER')).to.be.below(result.indexOf('AMENITY_EQUIPMENT_FAN'))
    }));

  })

})
