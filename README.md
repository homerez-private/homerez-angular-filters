# homerez-angular-filters 0.1.0

A collection of filters for Angular

## orderByTranslated
Very useful for when you have a ngRepeat over a list of translation keys that you want to have
sorted by its translated value. The filter returns the original list, but sorted on the translated
value of each key.

...more to come...