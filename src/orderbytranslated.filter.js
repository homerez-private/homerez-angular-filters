/**
 * orderByTranslated Filter
 * Sort ng-options or ng-repeat by translated values
 * @example
 *   ng-options="country as (country | translate) for country in countries | orderByTranslated"
 * @param  {Array} array
 * @param  {String} objKey
 * @return {Array}
 */
(function() {
  'use strict';

  angular.module('homerez.filters', ['pascalprecht.translate'])
    .filter('orderByTranslated', orderByTranslated);

  orderByTranslated.$inject = ['$translate', '$filter'];

  function orderByTranslated($translate, $filter) {
    return function(array, objKey) {
      var result = [];
      var translated = [];
      angular.forEach(array, function(value) {
        var translateKey = objKey ? value[objKey] : value;
        translated.push({
          key: value,
          label: $translate.instant(translateKey)
        });
      });
      angular.forEach($filter('orderBy')(translated, 'label'), function(sortedObject) {
        result.push(sortedObject.key);
      });
      return result;
    };
  }
}());
